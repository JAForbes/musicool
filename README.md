Musicool
========

A fast, minimalist music player.

Key goals:

- Simple: _Let me find the song and play it as fast as humanly possible.  No excuses!_
- Sexy: _Album art is the focus.  Application chrome is not._
- Invisble UI: _Context sensitive UI, invisible until you need it._
- Invisible Tagging: _Automatic and non destructive tagging of your collection._

##Automatic Tagging:

###Tagging Signatures:

Musicool generates a signature for every track it encounters.  
The signature is based on attributes of each track and its relationship with tracks in the same folder.

If you have untagged music in your library, Musicool will suggest tags based on other users tags.
This means by keeping your own library sexy you are saving every other user from repeating the same work.

###Wikipedia Scraping:

If the folder your music is contained in is called [Superunknown](http://en.wikipedia.org/wiki/Superunknown)
and the parent folder is called [Soundgarden](http://en.wikipedia.org/wiki/Soundgarden), Musicool will try to match 
the [track listing](http://en.wikipedia.org/wiki/Superunknown#Track_listing) from wikipedia to your files.

Musicool will also use album art it finds from Wikipedia.

###Non Destructive Tagging

Musicool is smart, and acts with authority.  It tags music without asking.  It pulls album art without asking.  
It rearranges your library and file names without asking.  
But none of these actions are destructive as Musicool stores its tag information with in YAML formatted `.tag` files.
Musicool will not write to ID3 unless you tell it to.

###Collection Directory

You can optionally enable the Collection directory feature.  This gives Musicool permission to move 
any album it deems to be "perfectly tagged" into a folder called `Collection`.  This helps to delineate which 
albums of your music collection require extra tagging work.
